
import 'package:redux_study_4/dto/picture_dto.dart';
import 'package:redux_study_4/dto/weather_dato.dart';
import 'package:redux_study_4/repositories/repository_interface.dart';
import 'package:redux_study_4/services/picture_service.dart';
import 'package:redux_study_4/services/weather_service.dart';

class PictureRepository implements RepositoryInterface{

  PictureRepository._privateConstructor();

  static final PictureRepository instance = PictureRepository._privateConstructor();


  @override
  Future <List<PictureDto>> getData() async{
    List<dynamic> responseData = await PictureService.instance.getPictures();
    List<PictureDto> pictures = [...responseData.map((e) => PictureDto.fromJson(e))];
    // pictures.add(PictureDto.fromJson(responseData[0]));
    return pictures;
  }
}