
import 'package:redux_study_4/dto/weather_dato.dart';
import 'package:redux_study_4/repositories/repository_interface.dart';
import 'package:redux_study_4/services/weather_service.dart';

class WeatherInfoRepository implements RepositoryInterface{

  WeatherInfoRepository._privateConstructor();

  static final WeatherInfoRepository instance = WeatherInfoRepository._privateConstructor();


  @override
  Future <WeatherDto> getData() async{
    Map<String, dynamic> responseData = await WeatherService.instance.getWeather();
    print('repo');
    WeatherDto weather =  WeatherDto.fromJson(responseData);
    return weather;
  }
}