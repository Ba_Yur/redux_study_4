import 'package:flutter/foundation.dart';

class PictureDto {
  final String author_url;


  PictureDto({
    @required this.author_url,

  });

  factory PictureDto.fromJson(Map<String, dynamic> json) {
    return PictureDto(
      author_url: json['download_url'],
    );
  }
}
