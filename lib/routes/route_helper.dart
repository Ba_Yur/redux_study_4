

import 'package:flutter/material.dart';
import 'package:redux_study_4/routes/routes.dart';
import 'package:redux_study_4/ui/picture_page.dart';
import 'package:redux_study_4/ui/weather_page/weather_page.dart';

import '../main.dart';


class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';
  RouteHelper._privateConstructor();
  static final RouteHelper _instance = RouteHelper._privateConstructor();
  static RouteHelper get instance => _instance;
  // endregion
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case weatherRoute:
        return _defaultRoute(
          settings: settings,
          page: MyHomePage(),
        );
      case mainPageRoute:
        return _defaultRoute(
          settings: settings,
          page: WeatherPage(),
        );
      default:
        return _defaultRoute(
          settings: settings,
          page: MyHomePage(),
        );
    }
  }
  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}