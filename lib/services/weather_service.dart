import 'dart:convert';

import 'package:http/http.dart' as http;

class WeatherService {

  WeatherService._privateConstructor();

  static final WeatherService instance = WeatherService._privateConstructor();

  static String _url = 'http://api.openweathermap.org/data/2.5/weather?q=Odessa&appid=bb974cb8fc3db752275482bdc29b6713';

  Future<Map<String, dynamic>> getWeather () async {
    var responseData = await http.get(_url);
    print('json');
    print(json.decode(responseData.body));
    return json.decode(responseData.body);
  }
}