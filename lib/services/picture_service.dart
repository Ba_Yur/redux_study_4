import 'dart:convert';

import 'package:http/http.dart' as http;

class PictureService {

  PictureService._privateConstructor();

  static final PictureService instance = PictureService._privateConstructor();

  static String _url = 'https://picsum.photos/v2/list?page=2&limit=10';

  // Future<Map<String, dynamic>> getPictures () async {
  //   var responseData = await http.get(_url);
  //   print(json.decode(responseData.body));
  //   print('json');
  //   return json.decode(responseData.body);
  // }
  Future<List<dynamic>> getPictures () async {
    var responseData = await http.get(_url);

    return json.decode(responseData.body);
  }

}