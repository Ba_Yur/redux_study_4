import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux_study_4/routes/route_helper.dart';
import 'package:redux_study_4/store/app_state.dart';
import 'package:redux_study_4/ui/picture_page.dart';
import 'package:redux_study_4/ui/picture_page_vm.dart';

void main() {
  Store store = Store<AppState>(
      AppState.getAppReducer,
      initialState: AppState.initial(),
      middleware: [
        EpicMiddleware(AppState.getAppEpic),
        NavigationMiddleware<AppState>(),
      ]
  );
  runApp(MyApp(store: store,));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  const MyApp({Key key, this.store}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: StoreConnector(
        converter: (Store<AppState> store) => store.state,
        builder: (BuildContext context, AppState state) {
          return MaterialApp(
          navigatorKey: NavigatorHolder.navigatorKey,
          onGenerateRoute: (RouteSettings settings) =>
              RouteHelper.instance.onGenerateRoute(settings),
          home: MyHomePage(),
        );
        },
      ),
    );
  }
}

