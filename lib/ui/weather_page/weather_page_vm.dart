

import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_4/dto/picture_dto.dart';
import 'package:redux_study_4/store/app_state.dart';
import 'package:redux_study_4/store/navigation_selector.dart';
import 'package:redux_study_4/store/pictures_state/pictures_selectors.dart';
import 'package:redux_study_4/store/weather_state/weather_selectors.dart';

class WeatherPageViewModel {
  final void Function() gotoMainPage;
  final String wind;
  final void Function() getWind;
  final String description;



  WeatherPageViewModel({
    @required this.gotoMainPage,
    @required this.wind,
    @required this.getWind,
    @required this.description,
  });

  static WeatherPageViewModel fromStore(Store<AppState> store) {
    return WeatherPageViewModel(
      gotoMainPage: NavigationSelector.navigateToMainPage(store),
        wind: WeatherSelector.getCurrentWeatherWind(store),
        getWind: WeatherSelector.getWeatherFunction(store),
        description : WeatherSelector.getCurrentWeatherDescription(store)
    );
  }
}

