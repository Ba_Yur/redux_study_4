


import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_study_4/ui/weather_page/weather_page_vm.dart';

class WeatherPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return StoreConnector(
      converter: WeatherPageViewModel.fromStore,
      builder: (BuildContext context, WeatherPageViewModel vm)=>Scaffold(
        appBar: AppBar(
          title: Text('Weather page'),
        ),
        body: Column(
          children: [
            Text('Wind speed in Odessa-city: ${vm.wind}',style: TextStyle(
                fontSize: 20
            ),),
            Text('Weather description: ${vm.description}',style: TextStyle(
                fontSize: 20
            ),),
            ElevatedButton(onPressed: (){vm.getWind();}, child: Text('Get weather')),
          ],
        ),
      ),
    );
  }
}
