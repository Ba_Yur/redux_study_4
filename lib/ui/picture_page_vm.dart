import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_4/dto/picture_dto.dart';
import 'package:redux_study_4/store/app_state.dart';
import 'package:redux_study_4/store/navigation_selector.dart';
import 'package:redux_study_4/store/pictures_state/pictures_selectors.dart';

class PicturesViewModel {
  final void Function() gotoWeatherPage;
  final void Function() getPictures;
  final List<PictureDto> list;


  PicturesViewModel({
    @required this.gotoWeatherPage,
    @required this.getPictures,
    @required this.list,
  });

  static PicturesViewModel fromStore(Store<AppState> store) {
    return PicturesViewModel(
      gotoWeatherPage: NavigationSelector.navigateToWeatherPage(store),
      list: store.state.pictureState.picturesList,
      getPictures: PictureSelector.getPicturesFunction(store),
    );
  }
}