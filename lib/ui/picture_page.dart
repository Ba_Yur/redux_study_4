import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_study_4/ui/picture_page_vm.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector(
      converter: PicturesViewModel.fromStore,
      builder: (BuildContext context, PicturesViewModel vm) {
        return Scaffold(
        appBar: AppBar(
          title: Text('Redux study practice'),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ElevatedButton(onPressed: () {vm.getPictures();}, child: Text('get pictures')),
              ElevatedButton(onPressed: () {vm.gotoWeatherPage();}, child: Text('go to weather page')),
              vm.list.isNotEmpty
                  ? Container(
                height: 500.0,
                    child: ListView(
                      itemExtent: 200.0,
                        children: [
                          ...vm.list.map((e) => Image.network(e.author_url)).toList(),
                        ],
                      ),
                  )
                  : SizedBox(),
            ],
          ),
        ),
      );
      },
    );
  }
}
