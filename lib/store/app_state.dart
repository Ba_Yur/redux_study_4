import 'package:flutter/foundation.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux_study_4/store/pictures_state/pictures_epics.dart';
import 'package:redux_study_4/store/pictures_state/pictures_state.dart';
import 'package:redux_study_4/store/weather_state/weather_epics.dart';
import 'package:redux_study_4/store/weather_state/weather_state.dart';

class AppState {
  final WeatherState weatherState;
  final PictureState pictureState;
  // final PhotosState photosState;


  AppState({
    @required this.weatherState,
    @required this.pictureState,
  });

  factory AppState.initial() {
    return AppState(
      weatherState: WeatherState.initial(),
      pictureState: PictureState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      weatherState: state.weatherState.reducer(action),
      pictureState: state.pictureState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    WeatherEpics.indexEpic,
    PicturesEpics.indexEpic
  ]);

}