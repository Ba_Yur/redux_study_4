
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_4/routes/routes.dart';

import 'app_state.dart';

class NavigationSelector {
  static void Function() navigateToMainPage(Store<AppState> store) {
    return () => store.dispatch(NavigateToAction.push(weatherRoute));
  }

  static void Function() navigateToWeatherPage(Store<AppState> store) {
    return () => store.dispatch(NavigateToAction.push(mainPageRoute));
  }
}
