import 'package:flutter/material.dart';
import 'package:redux_study_4/dto/picture_dto.dart';
import 'package:redux_study_4/dto/weather_dato.dart';

class GetPicturesAction {
  GetPicturesAction();
}

class SetPicturesAction{
  final List<PictureDto> picturesList;

  SetPicturesAction({@required this.picturesList});
}
