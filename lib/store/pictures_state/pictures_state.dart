import 'dart:collection';

import 'package:redux_study_4/dto/picture_dto.dart';
import 'package:redux_study_4/store/pictures_state/pictures_actions.dart';
import 'package:redux_study_4/store/reducer.dart';
import 'package:redux_study_4/store/weather_state/weather_actions.dart';

class PictureState {
  final List<PictureDto> picturesList;

  PictureState({
    this.picturesList,
  });

  factory PictureState.initial() => PictureState(picturesList: []);

  PictureState copyWith({List<PictureDto> picturesList}) {
    return PictureState(
        picturesList: picturesList?? this.picturesList,
    );
  }

  PictureState reducer(dynamic action) {
    return Reducer<PictureState>(
      actions: HashMap.from({
        SetPicturesAction: (dynamic action) =>
            setPicture((action as SetPicturesAction).picturesList),
      }),
    ).updateState(action, this);
  }

  PictureState setPicture(List<PictureDto> list) {
    print('save');
    print(list.length);
    return copyWith(
        picturesList: list,
    );
  }
}
