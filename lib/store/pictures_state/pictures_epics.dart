import 'package:redux_epics/redux_epics.dart';
import 'package:redux_study_4/dto/picture_dto.dart';
import 'package:redux_study_4/dto/weather_dato.dart';
import 'package:redux_study_4/repositories/picture_repository.dart';
import 'package:redux_study_4/repositories/weather_repository.dart';
import 'package:redux_study_4/store/pictures_state/pictures_actions.dart';
import 'package:redux_study_4/store/weather_state/weather_actions.dart';
import 'package:rxdart/rxdart.dart';

import '../app_state.dart';

class PicturesEpics {
  static final indexEpic = combineEpics<AppState>([
    getPictureEpic,
  ]);

  static Stream<dynamic> getPictureEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    print('PicturesEpic');
    return actions.whereType<GetPicturesAction>().switchMap(
          (GetPicturesAction action) async* {
            final List<PictureDto> list = await PictureRepository.instance.getData();
            print(list.length);
            yield SetPicturesAction(picturesList: list);
      },
    );
  }
}
