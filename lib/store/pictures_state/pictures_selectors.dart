
import 'package:redux/redux.dart';
import 'package:redux_study_4/dto/picture_dto.dart';
import 'package:redux_study_4/store/pictures_state/pictures_actions.dart';
import 'package:redux_study_4/store/weather_state/weather_actions.dart';

import '../app_state.dart';


class PictureSelector {
  static void Function() getPicturesFunction(Store<AppState> store) {
    print('getting pictures selectore');
    return () => store.dispatch(GetPicturesAction());
  }

static List<PictureDto> pictureList(Store<AppState> store) {
  print ('Picture list selector');
    print (store.state.pictureState.picturesList);
    return store.state.pictureState.picturesList;
}
}