import 'package:redux_epics/redux_epics.dart';
import 'package:redux_study_4/dto/weather_dato.dart';
import 'package:redux_study_4/repositories/weather_repository.dart';
import 'package:redux_study_4/store/weather_state/weather_actions.dart';
import 'package:rxdart/rxdart.dart';

import '../app_state.dart';

class WeatherEpics {
  static final indexEpic = combineEpics<AppState>([
    setWeatherEpic,
  ]);

  static Stream<dynamic> setWeatherEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    print('aaaaaa');
    return actions.whereType<GetWeatherAction>().switchMap(
          (action) {
        print('asd');
        return Stream.fromFuture(
          WeatherInfoRepository.instance.getData().then(
                (WeatherDto weather) {
              print('weather');
              return SaveWeatherAction(
                weather: weather,
              );
            },
          ),
        );
      },
    );
  }
}
