import 'package:flutter/material.dart';
import 'package:redux_study_4/dto/weather_dato.dart';

class GetWeatherAction {
  GetWeatherAction();
}

class SaveWeatherAction{
  final WeatherDto weather;

  SaveWeatherAction({@required this.weather});
}
