
import 'package:redux/redux.dart';
import 'package:redux_study_4/store/weather_state/weather_actions.dart';

import '../app_state.dart';


class WeatherSelector {
  static void Function() getWeatherFunction(Store<AppState> store) {
    print('getting weather');
    return () => store.dispatch(GetWeatherAction());
  }

  static String getCurrentWeatherWind(Store<AppState> store) {
    print ('store.state.weatherPageState.windSpeed');
    return store.state.weatherState.windSpeed;
  }

  static String getCurrentWeatherDescription(Store<AppState> store) {
    print ('store.state.weatherPageState.description');
    return store.state.weatherState.description;
  }
}